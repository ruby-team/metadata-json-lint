Source: metadata-json-lint
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Gabriel Filion <gabster@lelutin.ca>
# python3-docutils is required for the rst2man binary when building the man page
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake,
               ruby,
               ruby-json-schema (>= 2.8),
               ruby-rspec,
               ruby-spdx-licenses (>= 1.0),
               ruby-semantic-puppet,
               python3-docutils
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/ruby-team/metadata-json-lint.git
Vcs-Browser: https://salsa.debian.org/ruby-team/metadata-json-lint
Homepage: https://github.com/voxpupuli/metadata-json-lint
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: metadata-json-lint
Architecture: all
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: Utility to verify Puppet metadata.json files
 This tool lets users verify that a file in Puppet modules, metadata.json,
 conforms to the format that it is expected to have.
 .
 The metadata.json file is used when publishing a module to the public module
 repository on forge.puppet.com, or to a private module forge. With this
 information, you can expose dependencies to other modules, set Puppet version
 requirements and expose the module's license, author and other such metadata.
 .
 metadata-json-lint can be used directly, or it can be used through
 puppet-development-kit.
